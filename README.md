* The file *exp_main.py* is the main file.
    * Loops over each dataset
    * Loops over each beta
    * and call the function *experimental_design*
    
* The file *experimental_design.py*.
    * This function is called by *exp_main.py*
    * It repeat 100 times the main loop
        * At each loop, the dataset in first randomly splitted in two
        * prior probability shift is added in the training set
        * then it loops over each quantitative learning method

* *adjustment_acc.py*, *adjustment_cde.py*, *adjustment_em.py*,  *adjustment_oracle_bayes.py* contains quantification learning algorithms

* *bayes.py* adaptation of a classifier for a known new prior 