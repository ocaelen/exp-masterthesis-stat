import numpy as np
from scipy.stats import itemfreq
from sklearn.metrics import confusion_matrix 
import pdb
from sklearn.ensemble import RandomForestClassifier
from bayes import bayes

def crossval(Xtr, Ytr, modeltype, K):
    
    ## I don't like pandas' Dataframes !
    Xtr = Xtr.values
    Ytr = Ytr.values
        
    nbobs = len(Ytr)
    size_cv = int(nbobs/K)
    
    y_uniqueval = itemfreq(Ytr)[:,0]

    res = np.full((len(y_uniqueval),len(y_uniqueval)),0.0)

    # Mixes the data
    shuffled_i = np.random.permutation(len(Ytr))
    Xtr = Xtr[shuffled_i,:]
    Ytr = Ytr[shuffled_i]

    for k in range(K):
        
        # index for spliting the data "K" times
        idx1 = (k*size_cv)
        idx2 = ((k+1)*size_cv)
        if k == K-1:
            idx2 = Xtr.shape[0]
        idx = range(idx1, idx2)  # Validation
        idx_ = np.setdiff1d(range(nbobs), idx)
        
        m_cv = modeltype.fit(Xtr[idx_], Ytr[idx_])
        pred_cv = m_cv.predict_proba(Xtr[idx])
        pred_cv = y_uniqueval[np.apply_along_axis(lambda x: np.argmax(x) ,1,pred_cv)]
                
        res = res + confusion_matrix(Ytr[idx], pred_cv, labels=y_uniqueval)
        
    return res



def adjustment_acc(pm, ts, tr_, modeltype, y_uniqueval):
        
    ## apply the model on the testing data
    pred = pm.predict_proba(ts["X"])
    pred = y_uniqueval[np.apply_along_axis(lambda x: np.argmax(x) ,1,pred)]
    
    ## Trick to force to have all the output modalities (even when nb pred is 0)
    prop1 = itemfreq(np.append(pred, y_uniqueval))[:,1]#.astype(int)
    prop1 = prop1 - 1
    prop1 = prop1/sum(prop1)
    
    ## ASSERT (normally it must never be True)
    if(~(itemfreq(np.append(pred, y_uniqueval))[:,0] == y_uniqueval).all()):
        print("ERROR: The two list of string must be the same!!!")
        
    cm = crossval(tr_["X"], tr_["Y"], modeltype, 5)

    prop2 = (cm.T/np.sum(cm,1)).T
    
    try:
        prop3 = np.linalg.inv(prop2)
    except np.linalg.linalg.LinAlgError:
#        print("Singular matrix")
        prior_acc = np.full(prop1.shape[0],-1.0)  ## replace prior by "-1"
    else:
        prior_acc = np.dot(prop3, prop1)
        if not((prior_acc > 0).all() and (prior_acc < 1).all()):
#            print("Prior is not a probability")
            ## all probabilities are not in [0,1]
            ## replace prior by "-2"
            prior_acc = np.full(len(prior_acc),-2.0)

    if(prior_acc[0] >= 0 and prior_acc[0] <=1):  
        pm = modeltype.fit(tr_["X"], tr_["Y"])
        pred_acc = bayes(pm.predict_proba(ts["X"]), 
                         tr_["Y"], 
                         prior_acc, 
                         y_uniqueval)
    else: ## we did not manage to estimate the prior in the testing
        pred_acc = np.full(len(ts["Y"]), np.nan)
        
    return(pred_acc, 
           prior_acc)