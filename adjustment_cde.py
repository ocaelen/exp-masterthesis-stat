import pdb
import numpy as np
from bayes import bayes
from scipy.stats import itemfreq

def costlearing(Xtr, Ytr, Xts, dmr, positiveclass, modeltype):
    weight = np.full(len(Ytr),1.)
    weight[Ytr == positiveclass] = dmr**(-1)
    pm = modeltype.fit(Xtr, Ytr, weight)
    return(pm)


def adjustment_cde(pm, ts, tr_, modeltype, y_uniqueval, nbloop=50):
    if len(y_uniqueval) != 2:
        pred_cde = np.full(len(ts["Y"]), np.nan)
        prior_cde = np.full(len(y_uniqueval), -3.0)
        return (pred_cde, prior_cde)
    
    
    positiveclass = y_uniqueval[0] ## Positive class is the first one
    Ytr = tr_["Y"].values
    B = sum(Ytr==positiveclass)/sum(Ytr!=positiveclass)
    
    for i in range(nbloop):
        pred = pm.predict(ts["X"])
        
        ## On evite les valeurs extrme de A
        if sum(pred!=positiveclass) == 0:
            A = sum(pred==positiveclass)
        else:
            A = sum(pred==positiveclass)/sum(pred!=positiveclass)
            
        if A == 0:
            A = 0.001
        if A > 400:
            A = 400
        
        
        dmr = B/A
        pm = costlearing(tr_["X"], tr_["Y"], ts["Y"], dmr, positiveclass, modeltype)
        
    p = A/(A+1)    
    prior_cde = np.full(2,(p,1-p))        
    pred_cde = bayes(pm.predict_proba(ts["X"]), 
                         tr_["Y"], 
                         prior_cde, 
                         y_uniqueval)
    return(pred_cde, 
           prior_cde)