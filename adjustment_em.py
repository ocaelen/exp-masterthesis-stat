#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import numpy as np
import pdb
from scipy import stats
from sklearn.metrics import confusion_matrix
from sklearn.metrics import precision_score


def confmatrix (y_target, y_pred):
    uniqueval = set(y_pred+y_target)
    uniqueval = list(uniqueval)
    uniqueval.sort()
    
    res = np.full((len(uniqueval),len(uniqueval)), np.nan)
    for i,ii in enumerate(uniqueval):
        for j,jj in enumerate(uniqueval):
            ttt1 = np.where(np.array(y_target) == ii)[0]
            ttt2 = np.where(np.array(y_pred) == jj)[0]
            res[i,j] = len(set(ttt1).intersection(ttt2))
    
    precision = np.full(len(uniqueval),np.nan)
    recall = np.full(len(uniqueval),np.nan)
    fscore = np.full(len(uniqueval),np.nan)
    
    for i in range(len(uniqueval)):
        precision[i] = res[i,i]/(sum(res[:,i])+0.000000001)
        recall[i] = res[i,i]/(sum(res[i,:])+0.000000001)
        fscore[i] = 2*(precision[i] * recall[i])/(precision[i] + recall[i]+0.000001)
    
    return (res, precision, recall, fscore,  uniqueval)




def adjustment_em(pm, ts, tr_, y_uniqueval, nbLoops=50):

        
    ## Prediction on the testing set
    pred = pm.predict_proba(ts["X"])

    ifreq =  stats.itemfreq(tr_["Y"])
    empiricalPrior =  ifreq[:,1]/sum(ifreq[:,1])
    
    prior_em = empiricalPrior ## initialized by prior in training
    for ss in range(nbLoops):
        alpha = prior_em/empiricalPrior
        denominator = (alpha*pred).sum(axis=1)

        # Trick to transform the vector "denominator" to a matrix "dd"
        dd = denominator.reshape((-1,1))
        dd = np.repeat(dd, len(y_uniqueval),axis=1)
      
        pred_ = alpha*pred/dd  ## Adjusted predictions (testing)
        prior_em = pred_.mean(axis=0) ## new prior estimation
        
    pred_em = y_uniqueval[np.apply_along_axis(lambda x: np.argmax(x) ,1,pred_)]
        
    return(pred_em, ## prediction with adjustement (end of loop)
           prior_em ## estimation of the new prior probability (end of loop)     
           )
    
    


def adjustment_em_stop(pm, ts, tr_, y_uniqueval, nbLoops=50):
        
    pred = pm.predict_proba(ts["X"])  ## Prediction on the testing set
    pred_tr = pm.predict_proba(tr_["X"])  ## Prediction on the training set 
    
    ifreq =  stats.itemfreq(tr_["Y"])
    empiricalPrior =  ifreq[:,1]/sum(ifreq[:,1])
    
    estim_realPrior = np.full((len(y_uniqueval),nbLoops+1),np.nan)
    estim_realPrior[:,0] = empiricalPrior ## initialized by prior in training

    pred_ = list() ## Adjusted predictions on the test set
    
    acc_on_tr = np.full(nbLoops, np.nan)

    
    pred_test_best = np.full(len(ts["Y"]), np.nan)
    ac_precis_best = 0 

    for ss in range(nbLoops):

        alpha = estim_realPrior[:,ss]/empiricalPrior
        denominator = (alpha*pred).sum(axis=1)
        denominator_tr = (alpha*pred_tr).sum(axis=1)
                
        # Trick to transform the vector "denominator" to a matrix "dd"
        dd = denominator.reshape((-1,1))
        dd = np.repeat(dd, len(y_uniqueval),axis=1)

        dd_tr = denominator_tr.reshape((-1,1))
        dd_tr = np.repeat(dd_tr, len(y_uniqueval),axis=1)

        pred_.append(alpha*pred/dd)  ## Adjusted predictions (testing)
        
        ## Predictions on the training set (transform the prob in classes)
        pred_tr_ = alpha*pred_tr/dd_tr  ## Adjusted predictions (training)
        pred_tr_ = np.apply_along_axis(lambda x: np.argmax(x) ,1,pred_tr_)
        pred_tr_ = y_uniqueval[pred_tr_]

        acc_on_tr[ss] = precision_score(tr_["Y"].values.tolist(), 
                                        pred_tr_.tolist(), 
                                        average='weighted')
        
        estim_realPrior[:,ss+1] = pred_[ss].mean(axis=0) ## new prior estimation
        
        if ac_precis_best <= acc_on_tr[ss]: 
            ac_precis_best = acc_on_tr[ss]
            pred_test_best = pred_[ss]
            best_prior_estim = estim_realPrior[:,ss+1]

    pred_test_best = y_uniqueval[np.apply_along_axis(lambda x: np.argmax(x) ,1,pred_test_best)]

    return(pred_test_best, ## prediction with adjustement (max precision on tr)
           best_prior_estim ## estimation of the new prior probability (max precision on tr)
           )
    
    
## This version returns only the number of steps in the main loop
def adjustment_em_stop_counting(pm, ts, tr_, y_uniqueval, nbLoops=100):
        
    pred = pm.predict_proba(ts["X"])  ## Prediction on the testing set
    pred_tr = pm.predict_proba(tr_["X"])  ## Prediction on the training set 
    
    ifreq =  stats.itemfreq(tr_["Y"])
    empiricalPrior =  ifreq[:,1]/sum(ifreq[:,1])
    
    estim_realPrior = np.full((len(y_uniqueval),nbLoops+1),np.nan)
    estim_realPrior[:,0] = empiricalPrior ## initialized by prior in training

    pred_ = list() ## Adjusted predictions on the test set
    
    acc_on_tr = np.full(nbLoops, np.nan)

    
    pred_test_best = np.full(len(ts["Y"]), np.nan)
    ac_precis_best = 0 

    res = -1
    count_ttt = 0
    for ss in range(nbLoops):
        count_ttt += 1

        alpha = estim_realPrior[:,ss]/empiricalPrior
        denominator = (alpha*pred).sum(axis=1)
        denominator_tr = (alpha*pred_tr).sum(axis=1)
                
        # Trick to transform the vector "denominator" to a matrix "dd"
        dd = denominator.reshape((-1,1))
        dd = np.repeat(dd, len(y_uniqueval),axis=1)

        dd_tr = denominator_tr.reshape((-1,1))
        dd_tr = np.repeat(dd_tr, len(y_uniqueval),axis=1)

        pred_.append(alpha*pred/dd)  ## Adjusted predictions (testing)
        
        ## Predictions on the training set (transform the prob in classes)
        pred_tr_ = alpha*pred_tr/dd_tr  ## Adjusted predictions (training)
        pred_tr_ = np.apply_along_axis(lambda x: np.argmax(x) ,1,pred_tr_)
        pred_tr_ = y_uniqueval[pred_tr_]

        acc_on_tr[ss] = precision_score(tr_["Y"].values.tolist(), 
                                        pred_tr_.tolist(), 
                                        average='weighted')
        
        estim_realPrior[:,ss+1] = pred_[ss].mean(axis=0) ## new prior estimation
        
        if ac_precis_best <= acc_on_tr[ss]: 
            ac_precis_best = acc_on_tr[ss]
            pred_test_best = pred_[ss]
            best_prior_estim = estim_realPrior[:,ss+1]
            res = count_ttt

    pred_test_best = y_uniqueval[np.apply_along_axis(lambda x: np.argmax(x) ,1,pred_test_best)]

    return(res)
    
    
