from scipy.stats import itemfreq
from bayes import bayes
import numpy as np
import pdb

def adjustment_oracle_bayes(pm, ts, tr_, y_uniqueval):
    
    ## The oracle know the real prior in the testing 
    ifreq =  itemfreq(ts["Y"])
    prior_oracle_bayes = ifreq[:,1]/sum(ifreq[:,1])

    pred_oracle_bayes = bayes(pm.predict_proba(ts["X"]), 
                              tr_["Y"], 
                              prior_oracle_bayes,
                              y_uniqueval)

    return(pred_oracle_bayes, 
           prior_oracle_bayes)
    
