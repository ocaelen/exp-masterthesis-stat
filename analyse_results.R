rm(list=ls())
library(xtable)
library(PMCMRplus)



############
#### EM ####
############

stat_mse_em <- function() {
    dd <- read.csv2("~/tmp/StatMasterThesis_exp/csv/res_2_forR.csv")

    l_algo <- c("em", "em_stop", "base")
    l_model <- c("LogisticRegression", "RandomForestClassifier")
    l_beta <- seq(0.1, 0.9, by = 0.1)
    nbdataset <- 25

    val_nemenyi_all <- NULL
    algo_nemenyi_all <- NULL
    
        
    for (imodel in 1:length(l_model)){

        val_nemenyi_mod <- NULL
        algo_nemenyi_mod <- NULL
        
        res_mean <- matrix(nrow = length(l_beta), ncol = length(l_algo), dimnames=list(l_beta, l_algo))
        res_diff <- matrix(nrow = length(l_beta), ncol = length(l_algo), dimnames=list(l_beta, l_algo))
        
        tmp_val <- matrix(nrow = length(l_algo), ncol = nbdataset)
        for (ibeta in 1:length(l_beta)){
            dd_sub <- dd[dd$method %in% l_algo & dd$model == l_model[imodel] & dd$beta == l_beta[ibeta], -c(1,3,5,7)]
            
            for(ialgo in 1:length(l_algo)){
                val_mse <- dd_sub[dd_sub$method == l_algo[ialgo],3]
                val_mse <- as.numeric(as.character(val_mse)) ## factor2numeric
                tmp_val[ialgo,] <- val_mse
            }
            res_mean[ibeta,] <- apply(tmp_val, 1, mean)
            
            
            val <- c(tmp_val[1,],tmp_val[2,],tmp_val[3,])
            algo <- c( rep(l_algo[1],25) , rep(l_algo[2],25) , rep(l_algo[3],25)) 

            val_nemenyi_all <- c(val_nemenyi_all, val)
            algo_nemenyi_all <- c(algo_nemenyi_all, algo)

            val_nemenyi_mod <- c(val_nemenyi_mod, val)
            algo_nemenyi_mod <- c(algo_nemenyi_mod, algo)
            
                        
            ## cumpute the p val with the best1350
            ibest <- which.min(res_mean[ibeta,])
            for(ialgo in 1:length(l_algo)){
                res_diff[ibeta, ialgo] <- t.test(tmp_val[ialgo,],tmp_val[ibest,], paired = TRUE)$p.value < 0.05
            }
        }
        cat("\n\n")
        print(l_model[imodel])
        cat("\n")
        print(xtable(res_mean,digits=5))
        cat("\n")
        print("Diff from t.test")
        print(res_diff)

        df_nemenyi_all <- data.frame(val=val_nemenyi_all , algo=algo_nemenyi_all)
        mt_nemenyi_all <- cbind(
            df_nemenyi_all$val[df_nemenyi_all$algo == l_algo[1]],
            df_nemenyi_all$val[df_nemenyi_all$algo == l_algo[2]],
            df_nemenyi_all$val[df_nemenyi_all$algo == l_algo[3]])
        colnames(mt_nemenyi_all) <- l_algo
        
        
        
        df_nemenyi_mod <- data.frame(val=val_nemenyi_mod , algo=algo_nemenyi_mod)
        mt_nemenyi_mod <- cbind(
            df_nemenyi_mod$val[df_nemenyi_mod$algo == l_algo[1]],
            df_nemenyi_mod$val[df_nemenyi_mod$algo == l_algo[2]],
            df_nemenyi_mod$val[df_nemenyi_mod$algo == l_algo[3]])
        colnames(mt_nemenyi_mod) <- l_algo
        
        if (imodel == 3){
            summary(kwAllPairsNemenyiTest(val~algo, data= df_nemenyi_mod))
            round(tapply(df_nemenyi_mod$val, df_nemenyi_mod$algo, mean),4)
            friedmanTest(mt_nemenyi_mod)
            round(wilcox.test(mt_nemenyi_mod[,1],mt_nemenyi_mod[,3])$p.val,7) ## em - base
            round(wilcox.test(mt_nemenyi_mod[,2],mt_nemenyi_mod[,3])$p.val,7) ## em_stop - base
            round(wilcox.test(mt_nemenyi_mod[,2],mt_nemenyi_mod[,1])$p.val,4) ## em_stop - em
            browser()
        }
        
        
    }

    summary(kwAllPairsNemenyiTest(val~algo, data= df_nemenyi_all))
    round(tapply(df_nemenyi_all$val, df_nemenyi_all$algo, mean),4)
    friedmanTest(mt_nemenyi_all)
    
    round(wilcox.test(mt_nemenyi_all[,1],mt_nemenyi_all[,3])$p.val,7) ## em - base
    round(wilcox.test(mt_nemenyi_all[,2],mt_nemenyi_all[,3])$p.val,7) ## em_stop - base
    round(wilcox.test(mt_nemenyi_all[,2],mt_nemenyi_all[,1])$p.val,4) ## em_stop - em
    browser()    
}

stat_mse_em()




stat_f1_em <- function() {
    dd <- read.csv2("~/tmp/StatMasterThesis_exp/csv/res_2_forR.csv")
    
    l_algo <- c("em", "em_stop", "oracle_bayes", "oracle", "base")
    l_model <- c("LogisticRegression", "RandomForestClassifier")
    l_beta <- seq(0.1, 0.9, by = 0.1)
    nbdataset <- 25
    
    for (imodel in 1:length(l_model)){
        res_mean <- matrix(nrow = length(l_beta), ncol = length(l_algo), dimnames=list(l_beta, l_algo))
        res_diff <- matrix(nrow = length(l_beta), ncol = length(l_algo), dimnames=list(l_beta, l_algo))
        
        tmp_val <- matrix(nrow = length(l_algo), ncol = nbdataset)
        for (ibeta in 1:length(l_beta)){
            
            dd_sub <- dd[dd$method %in% l_algo & dd$model == l_model[imodel] & dd$beta == l_beta[ibeta], -c(1,3,5,6)]
            
            for(ialgo in 1:length(l_algo)){
                val_f1 <- dd_sub[dd_sub$method == l_algo[ialgo],3]
                val_f1 <- as.numeric(as.character(val_f1)) ## factor2numeric
                tmp_val[ialgo,] <- val_f1
            }
            
            res_mean[ibeta,] <- apply(tmp_val, 1, mean)
            
            ## cumpute the p val with the best (max!!!)
            ttt <- res_mean[ibeta,]
            ttt[c(3,4)] <- -1 ## je ne veux pas prendre oracle_bayes ou oracle
            ibest <- which.max(ttt)
            for(ialgo in 1:length(l_algo)){
                res_diff[ibeta, ialgo] <- t.test(tmp_val[ialgo,],tmp_val[ibest,], paired = TRUE)$p.value < 0.05
            }
        }
        cat("\n\n")
        print(l_model[imodel])
        cat("\n")
        print(xtable(res_mean,digits=4))
        cat("\n")
        print(res_diff)
    }
}






#############
#### CDE ####
#############

stat_mse_cde <- function() {
    dd <- read.csv2("~/tmp/StatMasterThesis_exp/csv/res_2_forR.csv")
    
    l_algo <- c("cde", "em", "em_stop", "base")
    l_model <- c("LogisticRegression", "RandomForestClassifier")
    l_beta <- seq(0.1, 0.9, by = 0.1)
    
    ## List of the dataset forwhich CDE give a solution (binary classification)
    dataset_cde <- as.character(unique(dd[dd$method=="cde" & dd$mse!="",]$dataset))
    nbdataset <- length(dataset_cde)
    
    for (imodel in 1:length(l_model)){
        
        res_mean <- matrix(nrow = length(l_beta), ncol = length(l_algo), dimnames=list(l_beta, l_algo))
        res_diff <- matrix(nrow = length(l_beta), ncol = length(l_algo), dimnames=list(l_beta, l_algo))
        
        tmp_val <- matrix(nrow = length(l_algo), ncol = nbdataset)
        for (ibeta in 1:length(l_beta)){
            dd_sub <- dd[dd$dataset %in% dataset_cde & 
                         dd$method %in% l_algo & 
                         dd$model == l_model[imodel] & 
                         dd$beta == l_beta[ibeta], 
                            -c(1,3,5,7)]
            
            for(ialgo in 1:length(l_algo)){
                val_mse <- dd_sub[dd_sub$method == l_algo[ialgo],3]
                val_mse <- as.numeric(as.character(val_mse)) ## factor2numeric
                tmp_val[ialgo,] <- val_mse
            }
            res_mean[ibeta,] <- apply(tmp_val, 1, mean)
            
            ## cumpute the p val with the best
            ibest <- which.min(res_mean[ibeta,])
            for(ialgo in 1:length(l_algo)){
                res_diff[ibeta, ialgo] <- t.test(tmp_val[ialgo,],tmp_val[ibest,], paired = TRUE)$p.value < 0.05
            }
        }
        browser()
        cat("\n\n")
        print(l_model[imodel])
        cat("\n")
        print(xtable(res_mean,digits=5))
        cat("\n")
        print("Diff from t.test")
        print(res_diff)
    }
}


stat_f1_cde <- function() {
    dd <- read.csv2("~/tmp/StatMasterThesis_exp/csv/res_2_forR.csv")
    
    l_algo <- c("cde","em", "em_stop", "oracle_bayes", "oracle", "base")
    l_model <- c("LogisticRegression", "RandomForestClassifier")
    l_beta <- seq(0.1, 0.9, by = 0.1)

    ## List of the dataset forwhich CDE give a solution (binary classification)
    dataset_cde <- as.character(unique(dd[dd$method=="cde" & dd$mse!="",]$dataset))
    nbdataset <- length(dataset_cde)
    
    for (imodel in 1:length(l_model)){
        res_mean <- matrix(nrow = length(l_beta), ncol = length(l_algo), dimnames=list(l_beta, l_algo))
        res_diff <- matrix(nrow = length(l_beta), ncol = length(l_algo), dimnames=list(l_beta, l_algo))
        
        tmp_val <- matrix(nrow = length(l_algo), ncol = nbdataset)
        for (ibeta in 1:length(l_beta)){
            
            dd_sub <- dd[dd$dataset %in% dataset_cde & 
                         dd$method %in% l_algo & 
                         dd$model == l_model[imodel] & 
                         dd$beta == l_beta[ibeta], 
                            -c(1,3,5,6)]
            
            for(ialgo in 1:length(l_algo)){
                val_f1 <- dd_sub[dd_sub$method == l_algo[ialgo],3]
                val_f1 <- as.numeric(as.character(val_f1)) ## factor2numeric
                tmp_val[ialgo,] <- val_f1
            }
            
            res_mean[ibeta,] <- apply(tmp_val, 1, mean)
            
            ## cumpute the p val with the best (max!!!)
            ttt <- res_mean[ibeta,]
            ttt[c(4,5)] <- -1 ## je ne veux pas prendre oracle_bayes ou oracle
            ibest <- which.max(ttt)
            for(ialgo in 1:length(l_algo)){
                res_diff[ibeta, ialgo] <- t.test(tmp_val[ialgo,],tmp_val[ibest,], paired = TRUE)$p.value < 0.1
            }
        }
        cat("\n\n")
        print(l_model[imodel])
        cat("\n")
        print(xtable(res_mean,digits=5))
        cat("\n")
        print(res_diff)
    }
}


#stat_mse_em() ## quantification -- for the section where EM is compared again the benchmark methods
#stat_f1_em() ## classification -- for the section where EM is compared again the benchmark methods


#stat_mse_cde() ## quantification -- for the section where EM is compared again the benchmark methods
#stat_f1_cde() ## classification -- for the section where EM is compared again the benchmark methods

