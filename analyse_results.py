#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import pickle
import pdb
import numpy as np
from read_data_classif import readdata
from sklearn.metrics import f1_score
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages
import pandas as pd



def analyse_results_step1(l_modeltype_names, 
                    l_dataset_names, 
                    l_algo_adjust_names, 
                    l_betas,
                    nbloops):  

    
    for i, dataset_name in enumerate(l_dataset_names): ## Loop over the datasets
        for j, modeltype_name in enumerate(l_modeltype_names):
            
            ## F1 score
            f1 = np.full((len(l_algo_adjust_names),
                          len(l_betas),
                          nbloops), np.nan)
            
            ## square error
            se = np.full((len(l_algo_adjust_names),
                          len(l_betas),
                          nbloops), np.nan)
            
            acc_nbSingular = np.full(len(l_betas), 0)
    
            acc_nbNoProb = np.full(len(l_betas), 0)
            
            cde_TooManyModal = np.full(len(l_betas), 0)
            
            
            for k, beta in enumerate(l_betas):
                
                with open("/home/ocaelen/tmp/StatMasterThesis_exp/pickle/res_" + 
                          modeltype_name + "_" + dataset_name + 
                          "_"+ "_beta"+  str(int(beta*100))+
                          ".pickle","rb") as file:
                    result = pickle.load(file)
                    
                    

                for l, algo_adjust in enumerate(l_algo_adjust_names):
                        
                    for loop in range(nbloops):
                        print("Step 1: "+
                              dataset_name + " " + 
                              modeltype_name + " " + 
                              str(beta) + " " + 
                              algo_adjust + " loop "+
                              str(loop))


                        name_pred = "l_pred_" + algo_adjust
                        name_prior = "l_prior_" + algo_adjust
                            
                            
                        if algo_adjust == "acc" and result[name_prior][loop][0] == -1.0:
                            acc_nbSingular[k] += 1
                        elif algo_adjust == "acc" and result[name_prior][loop][0] == -2.0:
                            acc_nbNoProb[k] += 1
                        elif algo_adjust == "cde" and result[name_prior][loop][0] == -3.0:
                            cde_TooManyModal[k] += 1
                        else:                                
                              
                            # l=algo_adjust, k=betas, loop
                            se[l,k,loop] =  np.mean((result['l_real_prior'][loop] - result[name_prior][loop])**2)

                            f1[l,k,loop] = f1_score(result["l_real_Y"][loop], 
                                                  result[name_pred][loop], 
                                                  average='weighted')   
                    
            with open("/home/ocaelen/tmp/StatMasterThesis_exp/pickle/res_2_" + 
                  dataset_name + "_" +modeltype_name+ "_se.pickle","wb") as file:
                pickle.dump(se, file)
            
            with open("/home/ocaelen/tmp/StatMasterThesis_exp/pickle/res_2_" + 
                  dataset_name + "_" +modeltype_name+ "_f1.pickle","wb") as file:
                pickle.dump(f1, file)
                
            with open("/home/ocaelen/tmp/StatMasterThesis_exp/pickle/res_2_" + 
                  dataset_name + "_" +modeltype_name+ "_nan.pickle","wb") as file:
                pickle.dump((acc_nbSingular,
                             acc_nbNoProb,
                             cde_TooManyModal), file)
                        





## Fait les graphique de l'apendix
def analyse_results_step2_f1_score(l_modeltype_names, 
                                   l_dataset_names, 
                                   l_algo_adjust_names, 
                                   l_betas):

    colors = ('b', 'g', 'r', 'c', 'm', 'y', 'k')

    for i, dataset_name in enumerate(l_dataset_names): ## Loop over the datasets
        for j, modeltype_name in enumerate(l_modeltype_names):
            
            with open("/home/ocaelen/tmp/StatMasterThesis_exp/pickle/res_2_" + 
                  dataset_name + "_" +modeltype_name+ "_f1.pickle","rb") as file:
                f1 = pickle.load(file)    
                
            with open("/home/ocaelen/tmp/StatMasterThesis_exp/pickle/res_2_" + 
                  dataset_name + "_" +modeltype_name+ "_nan.pickle","rb") as file:
                (acc_nbSingular,acc_nbNoProb, cde_TooManyModal) = pickle.load(file)
                        
            ## Test if at leat 10 success in each beta for ACC
            plotAcc = True
            if(sum(acc_nbSingular+acc_nbNoProb > 90)>0):
                plotAcc = False
                
            
            
            
            l_mean_f1 = np.full((len(l_algo_adjust_names),
                                 len(l_betas)), np.nan)
            
            for k, beta in enumerate(l_betas):            
                for l, algo_adjust in enumerate(l_algo_adjust_names):
                    if np.isnan(np.nanmean(f1[l,k,:])):
                        l_mean_f1[l,k] = 0       
                    else:    
                        l_mean_f1[l,k] = np.nanmean(f1[l,k,:])       
                                
            filename = "../../Latex/Figures/res/res_f1_" + modeltype_name + "_" + dataset_name + ".pdf"
            pdf = PdfPages(filename)

                      
            if np.sum(l_mean_f1[0,]) != 0 and plotAcc:
                plt.plot(l_betas, l_mean_f1[0,:], color=colors[0], label='acc', linewidth=2.0)
            if np.sum(l_mean_f1[1,]) != 0:
                plt.plot(l_betas, l_mean_f1[1,:], color=colors[1], label='cde', linewidth=2.0)            
            if np.sum(l_mean_f1[2,]) != 0:
                plt.plot(l_betas, l_mean_f1[2,:], color=colors[2], label='em', linewidth=2.0)
            if np.sum(l_mean_f1[3,]) != 0:
                plt.plot(l_betas, l_mean_f1[3,:], color=colors[3], label='em_stop', linewidth=2.0)
            if np.sum(l_mean_f1[4,]) != 0:
                plt.plot(l_betas, l_mean_f1[4,:], color=colors[4], label='oracle_bayes', linewidth=2.0)
            if np.sum(l_mean_f1[5,]) != 0:
                plt.plot(l_betas, l_mean_f1[5,:], color=colors[5], label='oracle', linewidth=2.0)
            if np.sum(l_mean_f1[6,]) != 0:
                plt.plot(l_betas, l_mean_f1[6,:], color=colors[6], label='base', linewidth=2.0)
            
            plt.xlabel("beta")
            plt.ylabel("f1-score")
            plt.title(dataset_name + " - " + modeltype_name)
            plt.legend()
            pdf.savefig()
            plt.close()
            pdf.close()
                



## Fait les graphique de l'apendix
def analyse_results_step2_mse(l_modeltype_names, 
                              l_dataset_names, 
                              l_algo_adjust_names, 
                              l_betas):

    colors = ('b', 'g', 'r', 'c', 'm', 'y', 'k')
    
    for i, dataset_name in enumerate(l_dataset_names): ## Loop over the datasets
        for j, modeltype_name in enumerate(l_modeltype_names):

            with open("/home/ocaelen/tmp/StatMasterThesis_exp/pickle/res_2_" + 
                  dataset_name + "_" +modeltype_name+ "_se.pickle","rb") as file:
                se = pickle.load(file)


            with open("/home/ocaelen/tmp/StatMasterThesis_exp/pickle/res_2_" + 
                  dataset_name + "_" +modeltype_name+ "_nan.pickle","rb") as file:
                (acc_nbSingular,acc_nbNoProb, cde_TooManyModal) = pickle.load(file)
                        
            ## Test if at leat 10 success in each beta for ACC
            plotAcc = True
            if(sum(acc_nbSingular+acc_nbNoProb > 90)>0):
                plotAcc = False

            
            ## for the plot
            l_mse = np.full((len(l_algo_adjust_names),
                             len(l_betas)), np.nan)
            
            for k, beta in enumerate(l_betas):            
                for l, algo_adjust in enumerate(l_algo_adjust_names):
                    l_mse[l,k] = np.nanmean(se[l,k,:])       
        
            filename = "../../Latex/Figures/res/res_mse_" + modeltype_name + "_" + dataset_name + ".pdf"
            pdf = PdfPages(filename)
            if ~np.isnan(l_mse[0,:].mean()) and plotAcc:
                plt.plot(l_betas, l_mse[0,:], color=colors[0], label="acc")
            if ~np.isnan(l_mse[1,:].mean()):    
                plt.plot(l_betas, l_mse[1,:], color=colors[1], label="cde")
            plt.plot(l_betas, l_mse[2,:], color=colors[2], label="em")
            plt.plot(l_betas, l_mse[3,:], color=colors[3], label="em_stop")
            #plt.plot(l_betas, l_mse[4,:], color=colors[4], label="oracle_bayes")
            #plt.plot(l_betas, l_mse[5,:], color=colors[5], label="oracle")
            plt.plot(l_betas, l_mse[6,:], color=colors[6], label="base")
            
            plt.xlabel("beta")
            plt.ylabel("MSE estime prior")
            plt.title(dataset_name + " - " + modeltype_name)
            plt.legend()
            pdf.savefig()
            plt.close()    
            pdf.close()

                
                
## Fait le tableau avec le nb d'erreur de ACC
def analyse_results_step2_acc(l_modeltype_names, 
                    l_dataset_names, 
                    l_algo_adjust_names, 
                    l_betas):

    print("")
    print("")
        
    for i, dataset_name in enumerate(l_dataset_names): ## Loop over the datasets
        for j, modeltype_name in enumerate(l_modeltype_names):

                
            with open("/home/ocaelen/tmp/StatMasterThesis_exp/pickle/res_2_" + 
                  dataset_name + "_" +modeltype_name+ "_nan.pickle","rb") as file:
                (acc_nbSingular,acc_nbNoProb, cde_TooManyModal) = pickle.load(file)
            
      
            
            #print(str(i)+" "+dataset_name+" - "+modeltype_name)
            
            
            if modeltype_name  == "LogisticRegression":    
                print("\hline")
                outstr = "\multirow{2}{*}{"+str(i+1)+"} & L "
            else:
                outstr = "& RF "
            for k in range(len(acc_nbSingular)):
                outstr = outstr + " & "
                outstr = outstr + str(acc_nbSingular[k])
                outstr = outstr + " & "
                outstr = outstr + str(acc_nbNoProb[k])
            outstr = outstr + " \\\\ "
            print(outstr)
    print("")
    print("")
                

## make the data for the R script
def analyse_results_step2_dataForR(l_modeltype_names, 
                                   l_dataset_names, 
                                   l_algo_adjust_names, 
                                   l_betas):

    col_method=[]
    col_model=[]
    col_dataset=[]
    col_beta=[]
    col_mse=[]
    col_f1=[]
    
    for i, dataset_name in enumerate(l_dataset_names): ## Loop over the datasets
        for j, modeltype_name in enumerate(l_modeltype_names):
            

            
            with open("/home/ocaelen/tmp/StatMasterThesis_exp/pickle/res_2_" + 
                      dataset_name + "_" +modeltype_name+ "_se.pickle","rb") as file:
                s_mse = pickle.load(file)
            
            with open("/home/ocaelen/tmp/StatMasterThesis_exp/pickle/res_2_" + 
                  dataset_name + "_" +modeltype_name+ "_f1.pickle","rb") as file:
                s_f1 = pickle.load(file)
            
            
            for k, beta in enumerate(l_betas):            
                for lll, algo_adjust in enumerate(l_algo_adjust_names):
                    print(dataset_name +" "+ 
                          modeltype_name +" "+ 
                          str(round(beta,2)) +" "+ 
                          algo_adjust)   

                    col_method.append(algo_adjust)
                    col_model.append(modeltype_name)
                    col_dataset.append(dataset_name)
                    col_beta.append(str(round(beta,2)))

                    col_mse.append(np.nanmean(s_mse[lll,k,:]))
                    col_f1.append(np.nanmean(s_f1[lll,k,:]))
                        
    res = { "method" : col_method,
            "model" : col_model,
            "dataset": col_dataset,
            "beta": col_beta,
            "mse": col_mse,
            "f1": col_f1
            }
    res = pd.DataFrame(res)
    res.to_csv("/home/ocaelen/tmp/StatMasterThesis_exp/csv/res_2_forR.csv", 
               sep=';',
               header=True,
               columns=["method","model","dataset","beta","mse","f1"])

    
    
    
    
    
    
if __name__ == "__main__":
    D = readdata(debug=True)
    dataset_names = list(map(lambda x: x["name"],D))
    algo_adjust_names = ("acc", 
                         "cde",
                         "em",
                         "em_stop", 
                         "oracle_bayes" , 
                         "oracle", 
                         "base")
    l_betas = np.linspace(0.1, 0.9, 9) 
    nbloops = 100  ## 100
    l_modeltype_names = ("LogisticRegression", "RandomForestClassifier")

    analyse_results_step2_dataForR(l_modeltype_names, 
                                    dataset_names,
                                    algo_adjust_names,
                                    l_betas)

 #   analyse_results_step1(l_modeltype_names, 
 #                         dataset_names,
 #                         algo_adjust_names,
 #                         l_betas,
 #                         nbloops)


    
#    analyse_results_step2_f1_score(l_modeltype_names, 
#                                   dataset_names,
#                                   algo_adjust_names,
#                                   l_betas)

#    analyse_results_step2_mse(l_modeltype_names, 
#                                   dataset_names,
#                                   algo_adjust_names,
#                                   l_betas)
    
    
#    analyse_results_step2_acc(l_modeltype_names, 
#                                   dataset_names,
#                                   algo_adjust_names,
#                                   l_betas)    
 
