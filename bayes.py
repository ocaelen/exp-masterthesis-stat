import pdb
import numpy as np
from scipy.stats import itemfreq

def bayes(pred, Ytr, prior_ts, y_uniqueval, debug=False):
    
    if debug:
        pdb.set_trace()
    
    ## Trick to force to have all the output modalities
    prior_tr = itemfreq(np.append(Ytr, y_uniqueval))[:,1]
    prior_tr = prior_tr - 1
    prior_tr = prior_tr/sum(prior_tr)    
    
    for i in range(pred.shape[0]):
        pred[i,:] = pred[i,:] * prior_ts / prior_tr
        pred[i,:] = pred[i,:]/sum(pred[i,:])
    
    pred = y_uniqueval[np.apply_along_axis(lambda x: np.argmax(x) ,1,pred)]
    
    return pred