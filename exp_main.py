#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from read_data_classif import readdata
from experimental_design import experimental_design
from analyse_results import analyse_results_step1
from analyse_results import analyse_results_step2_f1_score
from analyse_results import analyse_results_step2_mse
from analyse_results import analyse_results_step2_acc
from sklearn.linear_model import LogisticRegression
from sklearn.ensemble import RandomForestClassifier
import numpy as np

np.random.seed(1)
#random.seed(1)


def startExp(modeltype_name, nbloops, l_betas, D):

    ## Select the type of model
    if modeltype_name == "LogisticRegression":
        modeltype = LogisticRegression()
    elif modeltype_name == "RandomForestClassifier":  
        modeltype = RandomForestClassifier(n_estimators=200)

    ## Loop over the datasets and beta
    for dataset in D:
        for beta in l_betas:  
            print(modeltype_name +" -- "+ dataset["name"]+" -- Beta: "+str(beta))
            experimental_design(modeltype, 
                                dataset, 
                                beta, 
                                nbloops = nbloops,   
                                dataset_name = dataset["name"],
                                modeltype_name = modeltype_name,
                                algo_adjust_names = algo_adjust_names)




D = readdata(debug=True)
dataset_names = list(map(lambda x: x["name"],D))
algo_adjust_names = ("acc", 
                     "cde",
                     "em",
                     "em_stop", 
                     "oracle_bayes" , 
                     "oracle", 
                     "base")
l_betas = np.linspace(0.1, 0.9, 9) 
nbloops = 100  ## 100
l_modeltype_names = ("LogisticRegression", "RandomForestClassifier")

#for modeltype_name in l_modeltype_names:
#    startExp(modeltype_name, nbloops, l_betas, D)

#analyse_results_step1(l_modeltype_names, 
#                      dataset_names, 
#                      algo_adjust_names, 
#                      l_betas, 
#                      nbloops)

analyse_results_step2_mse(l_modeltype_names, 
                               dataset_names,
                               algo_adjust_names,
                               l_betas)


analyse_results_step2_f1_score(l_modeltype_names, 
                               dataset_names,
                               algo_adjust_names,
                               l_betas)



#analyse_results_step2_acc(l_modeltype_names, 
#                               dataset_names,
#                               algo_adjust_names,
#                               l_betas)    