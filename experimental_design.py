#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from read_data_classif import split_stratified_train_test
from read_data_classif import readdata
from sklearn.linear_model import LogisticRegression
from adjustment_em import adjustment_em
from adjustment_em import adjustment_em_stop
from adjustment_acc import adjustment_acc
from adjustment_oracle_bayes import adjustment_oracle_bayes
from adjustment_cde import adjustment_cde
import numpy as np
import random
import pdb
import pickle
from scipy.stats import itemfreq




def modify_prior_proba(tr, beta):
    tr_ = {}
    tr_["name"] = tr["name"]
        
    y_uniqueval = np.unique(tr["Y"])
    Omega = np.random.choice(y_uniqueval, 
                             size=random.randint(1, len(y_uniqueval)-1), 
                             replace=False)

    # Loop ove all the modalities
    idx_=[]
    for omega in y_uniqueval:
        idx = tr["Y"].index[tr["Y"] == omega].values
        np.random.shuffle(idx)
    
        # Check if 'omega' must be undersampled 
        if omega in Omega:
            iidx = max(int(len(idx)*beta),1) ## use max to avoid "0"
            idx_=idx_+list(idx[:iidx]) # add prior_proba on omega
        else:
            idx_=idx_+list(idx)
            
    # Creation of the new training data set
    tr_["X"] = tr["X"].loc[idx_]
    tr_["Y"] = tr["Y"].loc[idx_]
    
    return tr_
    
            
def experimental_design(modeltype, dataset, beta, nbloops, 
                        dataset_name, modeltype_name, algo_adjust_names):

    l_real_Y = list() ## The real output values Y in the testing-set (target)
    l_real_prior = list() ## The real prior in the testing (target)

    l_pred_acc = list() ## output Y predicted by ACC
    l_prior_acc = list() ## prior predicted by ACC

    l_pred_cde = list() ## output Y predicted by CDE
    l_prior_cde = list() ## prior predicted by CDE

    l_pred_em = list() ## output Y predicted by EM
    l_prior_em = list() ## prior predicted by EM
    
    l_pred_em_stop = list() ## output Y predicted by EM_stop
    l_prior_em_stop = list() ## prior predicted by EM_stop
    
    l_pred_oracle_bayes = list() ## output Y predicted by oracle_bayes
    l_prior_oracle_bayes = list() ## prior in ts -- it is not a prédiction!

    l_pred_oracle = list() ## output Y predicted by oracle
    l_prior_oracle = list() ## prior in ts -- it is not a prédiction!

    l_pred_base = list() ## output Y predicted without adjutment
    l_prior_base = list() ## prior in tr_  -- it is not a prédiction!
    
    # List of modalities 
    y_uniqueval = itemfreq(dataset["Y"])[:,0]

    for l in range(nbloops):
        print(" Loop:"+str(l))
        
        ## slit the dataset and modify the prior in the training set
        tr, ts = split_stratified_train_test(dataset)
        l_real_Y.append(ts["Y"].values)
        tr_ = modify_prior_proba(tr, beta)
        
        ## estmation of the real prior computed on output of the testing  
        ifreq =  itemfreq(ts["Y"])
        l_real_prior.append(ifreq[:,1]/sum(ifreq[:,1]))


        ## ACC adjustment
        pm = modeltype.fit(tr_["X"], tr_["Y"]) ## model learned on the new training set
        (pred_acc,
         prior_acc) = adjustment_acc(pm, ts, tr_, modeltype, y_uniqueval)
        l_pred_acc.append(pred_acc)
        l_prior_acc.append(prior_acc)


        
        ## CDE adjustment
        pm = modeltype.fit(tr_["X"], tr_["Y"]) ## model learned on the new training set
        (pred_cde,
         prior_cde) = adjustment_cde(pm, ts, tr_, modeltype, y_uniqueval)
        l_pred_cde.append(pred_cde) 
        l_prior_cde.append(prior_cde)


        ## EM adjustment
        pm = modeltype.fit(tr_["X"], tr_["Y"]) ## model learned on the new training set
        (pred_em, 
         prior_em) = adjustment_em(pm, ts, tr_, y_uniqueval)
        l_pred_em.append(pred_em)
        l_prior_em.append(prior_em)   
        
        
        
        ## EM adjustment        
        pm = modeltype.fit(tr_["X"], tr_["Y"]) ## model learned on the new training set
        (pred_em_stop, 
         prior_em_stop) = adjustment_em_stop(pm, ts, tr_, y_uniqueval)
        l_pred_em_stop.append(pred_em_stop)
        l_prior_em_stop.append(prior_em_stop)

        
        
        ## oracle_bayes adjustment
        pm = modeltype.fit(tr_["X"], tr_["Y"]) ## model learned on the new training set
        (pred_oracle_bayes,
         prior_oracle_bayes) = adjustment_oracle_bayes(pm, ts, tr_, y_uniqueval)
        l_pred_oracle_bayes.append(pred_oracle_bayes)
        l_prior_oracle_bayes.append(prior_oracle_bayes)
        
        
        
        
        ## oracle adjustment
        pm_oracle = modeltype.fit(tr["X"], tr["Y"]) ## original 'tr' dataset !!
        pred_oracle = y_uniqueval[np.apply_along_axis(lambda x: np.argmax(x) , 1, 
                                                    pm_oracle.predict_proba(ts["X"]))]
        l_pred_oracle.append(pred_oracle)
        l_prior_oracle.append(l_real_prior[l])
                

        ## use the "base" model
        pm = modeltype.fit(tr_["X"], tr_["Y"]) ## model learned on the new training set 
        pred_base = y_uniqueval[np.apply_along_axis(lambda x: np.argmax(x) , 1, 
                                                    pm.predict_proba(ts["X"]))]
        l_pred_base.append(pred_base)
        ifreq =  itemfreq(tr_["Y"])
        l_prior_base.append(ifreq[:,1]/sum(ifreq[:,1])) # prior in tr_


    ## 'res' is the output vector
    res = {"l_real_Y" : l_real_Y,
           "l_real_prior" : l_real_prior,
           "l_pred_acc" : l_pred_acc,
           "l_prior_acc" : l_prior_acc,
           "l_pred_cde" : l_pred_cde,
           "l_prior_cde" : l_prior_cde,
           "l_pred_em" : l_pred_em,
           "l_prior_em" : l_prior_em,
           "l_pred_em_stop" : l_pred_em_stop,
           "l_prior_em_stop" : l_prior_em_stop,
           "l_pred_oracle_bayes" : l_pred_oracle_bayes,
           "l_prior_oracle_bayes" : l_prior_oracle_bayes,
           "l_pred_oracle" : l_pred_oracle,
           "l_prior_oracle" : l_prior_oracle,
           "l_pred_base" : l_pred_base,
           "l_prior_base" : l_prior_base
            }
    
    with open("/home/ocaelen/tmp/StatMasterThesis_exp/pickle/res_" + 
              modeltype_name + "_" + dataset_name + 
              "_"+ "_beta"+  str(int(beta*100))+
              ".pickle","wb") as file:
        pickle.dump(res, file)
  
        
if __name__ == "__main__":
    print("Start")
    np.random.seed(4)
    #random.seed(1)

    D = readdata()
    
    ttt = experimental_design(LogisticRegression(), D[8], 0.01, nbloops=100,
                              dataset_name="TEST", modeltype_name="TEST", algo_adjust_names="TEST")
    print("End")
        
        
























