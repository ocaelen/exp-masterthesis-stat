#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from read_data_classif import readdata
from sklearn.linear_model import LogisticRegression
from sklearn.ensemble import RandomForestClassifier
from read_data_classif import split_stratified_train_test
import numpy as np
from experimental_design import modify_prior_proba
from adjustment_em import adjustment_em_stop_counting
from scipy.stats import itemfreq
import pickle
import matplotlib.pyplot as plt

np.random.seed(1)

D = readdata(debug=True)
l_betas = np.linspace(0.1, 0.9, 9)
l_modeltype_names = ("LogisticRegression", "RandomForestClassifier")
nbloops = 200  ## 100

dataset_names = list(map(lambda x: x["name"], D))

counting = np.zeros(shape=(len(D),
                           len(l_betas),
                           len(l_modeltype_names),
                           nbloops))

for idataset, dataset in enumerate(D):
    y_uniqueval = itemfreq(dataset["Y"])[:,0]
    for ibeta, beta in enumerate(l_betas):
        for imodeltype, modeltype_name in enumerate(l_modeltype_names):

            ## Select the type of model
            if modeltype_name == "LogisticRegression":
                modeltype = LogisticRegression()
            elif modeltype_name == "RandomForestClassifier":
                modeltype = RandomForestClassifier(n_estimators=200)

            for l in range(nbloops):
                print(modeltype_name +" -- "+
                      dataset["name"]+" -- Beta: "+
                      str(beta) + " Loop:"+
                      str(l))
                
                ## slit the dataset and modify the prior in the training set
                tr, ts = split_stratified_train_test(dataset)
                tr_ = modify_prior_proba(tr, beta)

                ## model learned on the new training set
                pm = modeltype.fit(tr_["X"], tr_["Y"])

                nbstep = adjustment_em_stop_counting(pm, ts, tr_, y_uniqueval)
                counting[idataset, ibeta, imodeltype, l] = nbstep



f_name = "/home/ocaelen/tmp/StatMasterThesis_exp/pickle/res_nbloops_em.pickle"
#with open(f_name,"wb") as file:
#    pickle.dump(counting, file)
with open(f_name,"rb") as file:
    counting = pickle.load(file)
    
    
    

for imodeltype, modeltype_name in enumerate(l_modeltype_names):
    mu = np.empty(shape=9)
    sigma = np.empty(shape=9)
    for ibeta, beta in enumerate(l_betas):
        values = counting[:,ibeta,imodeltype,:].reshape((1,-1))[0]
        mu[ibeta] = np.mean(values)
        sigma[ibeta] = np.std(values)
    plt.plot(l_betas, mu, label=modeltype_name)
    plt.fill_between(l_betas, 
                     mu - sigma * 1.96 / np.sqrt(200),
                     mu + sigma * 1.96 / np.sqrt(200),
                     alpha = 0.4)

plt.xlabel("beta")
plt.ylabel("Mean nb loops (CI 0.95)")
plt.title("Mean of the number of loops done by 'em-stop'")
plt.legend()

plt.show()

    
        
        
        
        
        