    # -*- coding: utf-8 -*-

import pandas as pd
from pandas.api.types import is_numeric_dtype
import numpy as np
from scipy.stats import itemfreq
from sklearn.preprocessing import OneHotEncoder
from sklearn.preprocessing import LabelEncoder
import pdb

def readdata(debug=False):
    def readelem(name, extfile, skiprows=None, sep=",", decimal=".", outputlast = True, 
                 na_values=None, index_col=None):
        
        df = pd.read_csv("../data/"+name+extfile,
                         header=None,
                         skiprows=skiprows,
                         decimal=decimal,
                         sep=sep,
                         na_values=na_values,
                         index_col=index_col)

        nbcol = len(df.columns)
        elem = {}
        elem["name"] = name
        if outputlast:
            elem["Y"] = df.iloc[:, -1]
            elem["X"] = df.iloc[:, 0:(nbcol-1)]
        else: # output is the first variable
            elem["Y"] = df.iloc[:, 0]
            elem["X"] = df.iloc[:, 1:nbcol]
        return elem

    res = list()  # It will be a list of dictionary
    res.append(readelem("abalone", ".data", outputlast=False))  
    res.append(readelem("balance-scale", ".data", outputlast=False))
    res.append(readelem("data_banknote_authentication", ".txt"))
    res.append(readelem("transfusion", ".data", 1))
    res.append(readelem("BreastTissue", ".csv", decimal=",",outputlast=False))
    res.append(readelem("default_of_credit_card_clients", ".csv" )) ## Delete the first column
    res.append(readelem("dermatology", ".data", na_values="?"))
    res.append(readelem("ecoli_2", ".data"))
    res.append(readelem("glass", ".data")) ## Delete the first column
    res.append(readelem("haberman", ".data")) 
    res.append(readelem("HTRU_2", ".csv")) 
    res.append(readelem("segmentation", ".data",outputlast=False)) 
    res.append(readelem("iris", ".data")) 
    res.append(readelem("letter-recognition", ".data",outputlast=False)) 
    res.append(readelem("magic04", ".data")) 
    res.append(readelem("mammographic_masses", ".data", na_values="?")) 
    res.append(readelem("Mice_Protein_Data_Cortex_Nuclear", ".csv",decimal="," )) ## Delete the first column
    res.append(readelem("occupancy_data_datatraining", ".txt", skiprows=1)) ## Delete the 2 first columns
    res.append(readelem("page-blocks_", ".csv"  ))
    res.append(readelem("pendigits", ".tra"   ))
    res.append(readelem("sonar", ".all-data"))
    res.append(readelem("seismic-bumps", ".arff"))
    res.append(readelem("spambase", ".data"))
    res.append(readelem("waveform", ".data"))
    res.append(readelem("wine", ".data",outputlast=False)) 
    

    res[5]["X"] = res[5]["X"].drop(0, axis=1) ## remove a column
    res[7]["X"] = res[7]["X"].drop(0, axis=1) ## remove a column
    res[8]["X"] = res[8]["X"].drop(0, axis=1) ## remove a column
    res[11]["X"] = res[11]["X"].drop(3, axis=1) ## remove a column
    res[16]["X"] = res[16]["X"].drop(0, axis=1) ## remove a column
    res[17]["X"] = res[17]["X"].drop(0, axis=1).drop(1, axis=1) ## remove 2 column
    res[21]["X"] = res[21]["X"].drop(13, axis=1).drop(14, axis=1).drop(15, axis=1)
    
        

    for D in res:

        # Remove rows with missing values
        row_tobe_remove = D["X"].isnull().apply(lambda x: any(x), axis=1)
        row_tobe_remove = row_tobe_remove.index[row_tobe_remove]
        D["X"] = D["X"].drop(row_tobe_remove)  # Remove observations with NaN
        D["Y"] = D["Y"].drop(row_tobe_remove)
        if len(row_tobe_remove) > 0:
            print(D["name"]+" has "+str(len(row_tobe_remove))+" missing (input)")

        row_tobe_remove = D["Y"].isnull()
        row_tobe_remove = row_tobe_remove.index[row_tobe_remove]
        D["X"] = D["X"].drop(row_tobe_remove)  # Remove observations with NaN
        D["Y"] = D["Y"].drop(row_tobe_remove)
        if len(row_tobe_remove) > 0:
            print(D["name"]+" has "+str(len(row_tobe_remove))+" missing (output)")

        for ncol in D["X"]:
            v = D["X"][ncol]
            if is_numeric_dtype(v):
                D["X"][ncol] = (v-v.mean())/v.std()  # Normalization

        # Mixes the data
        shuffled_i = np.random.permutation(len(D["X"]))
        D["X"] = D["X"].iloc[shuffled_i]
        D["Y"] = D["Y"].iloc[shuffled_i]
        
        if debug and D["X"].shape[0] > 20000:
            print("DEBUG!!!!")
            D["X"] = D["X"].head(20000)
            D["Y"] = D["Y"].head(20000)

    ## Only takes vowels (to reduce the size of the dataset)
    isvowel = np.isin(res[13]["Y"],["A","E","I","O","U","Y"])
    res[13]["Y"] = res[13]["Y"][isvowel]
    res[13]["X"] = res[13]["X"][isvowel]

    ## Transform discrete variables to numeric 
    res[16]["X"][78] = LabelEncoder().fit_transform(res[16]["X"][78])
    res[16]["X"][79] = LabelEncoder().fit_transform(res[16]["X"][79])
    res[16]["X"][80] = LabelEncoder().fit_transform(res[16]["X"][80])
    res[21]["X"][0] = LabelEncoder().fit_transform(res[21]["X"][0])
    res[21]["X"][1] = LabelEncoder().fit_transform(res[21]["X"][1])
    res[21]["X"][2] = LabelEncoder().fit_transform(res[21]["X"][2])
    res[21]["X"][7] = LabelEncoder().fit_transform(res[21]["X"][7])


    #D = list()      ## DEBUG
    #D.append(res[14]) ## DEBUG
    #res = D         ## DEBUG

    return res


def split_stratified_train_test(D):
    # Mixes the data
    global idx
    shuffled_i = np.random.permutation(len(D["X"]))
    D["X"] = D["X"].iloc[shuffled_i]
    D["Y"] = D["Y"].iloc[shuffled_i]
    
    # Compute (with strat) the index of the testing and training samples
    train_i = []
    test_i = []        
    for k in D["Y"].unique():
        idx = D["Y"].index[D["Y"] == k].values
        test_size = int(len(idx) * 0.5)
        np.random.shuffle(idx)
        
        train_i = train_i + list(idx[:test_size])
        test_i = test_i + list(idx[test_size:])

    # Make the training set
    train = {}
    train["name"] = D["name"]
    train["Y"] = D["Y"].loc[train_i]
    train["X"] = D["X"].loc[train_i]

    # Make the testing set
    test = {}
    test["name"] = D["name"]
    test["Y"] = D["Y"].loc[test_i]
    test["X"] = D["X"].loc[test_i]

    return train, test

def stat_data(D):
    print()
    print("Stat data:")
    for d in D:
        print()
        print(d["name"])
        print("  Nb obs:   "+str(d["X"].shape[0]))
        print("  Input dim:"+str(d["X"].shape[1]))
        print("  Nb class: "+str(len(d["Y"].unique())))
    print()
        


if __name__ == "__main__":
    print("Start")
    D = readdata()
    stat_data(D)
    tr, ts = split_stratified_train_test(D[0])
    
    print(itemfreq(tr["Y"]))
    print(itemfreq(ts["Y"]))
        
    print("End")